import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import '../less/noteDetail.less'
import Header from "../../header/Header";
import {deleteNote} from "../actions/noteDetailAction";

class NoteDetail extends React.Component {

  deleteNote() {
    this.props.deleteNote(this.props.match.params.id);
  }

  render() {
    const notesList = this.props.notesList;
    const noteDetail = notesList.find(item => ((this.props.match.params.id) == item.id));
    const {title, description} = noteDetail;
    return (
      <section>
        <Header/>
        <div className={'NoteDetails'}>
          <div className={'sideBar'}>
            {
              notesList.map(item => {
                return <div className={`title ${item.title == title ? 'choose':''}`} key={item.id}><Link to={`/notes/${item.id}`}><span>{item.title}</span></Link></div>
              })
            }
          </div>
          <div className={'detail'}>
            <h3>{title}</h3>
            <p className={'line'}></p>
            <div className={'description'}>{description}</div>
          </div>
          <div className={'operation'}>
            <div className={'operationBlock'}>
              <div className={'delete'} onClick={this.deleteNote.bind(this)}><Link to={'/'}>删除</Link></div>
              <div className={'return'}><Link to={'/'}>返回</Link></div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    notesList: state.notes.notesList,
  }
};

const mapDispatchToProps = dispatch => bindActionCreators({
  deleteNote
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NoteDetail);