export const getNoteDetail = () => (dispatch) =>{
  dispatch({
    type: 'GET_DETAIL'
  });
};

export const deleteNote = (id) => (dispatch) => {
  fetch(`http://localhost:8080/api/posts/${id}`, {
    method: 'DELETE'
  }).then(res => {
    dispatch({
      type: 'DELETE_NOTE',
    });
  });
};