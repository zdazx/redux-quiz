const initState ={
  notesList: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_DETAIL':
      return {
        ...state
      };
    case 'DELETE_NOTE':
      return {
        ...state
      };
    default:
      return state;
  }
};