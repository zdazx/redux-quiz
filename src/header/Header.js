import React from "react";
import {MdEventNote} from 'react-icons/md';
import './less/Header.less'

const Header = () => {
  return (
    <header className={'Header'}>
      <MdEventNote className={'icon'} />
      <h2 className={'headline'}>NOTES</h2>
    </header>
  );
};

export default Header;