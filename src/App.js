import React, {Component} from 'react';
import './App.less';
import {Route, BrowserRouter as Router, Switch} from "react-router-dom";
import Home from "./home/pages/Home";
import NoteDetail from "./noteDetail/pages/NoteDetails";
import NoteCreate from "./noteCreate/pages/NoteCreate";

class App extends Component{
  render() {
    return (
      <div className='App'>
        <Router>
          <Switch>
            <Route path={'/notes/create'} component={NoteCreate} />
            <Route path={'/notes/:id'} component={NoteDetail} />
            <Route path="/" component={Home} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;