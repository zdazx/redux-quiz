import React from 'react';
import Header from "../../header/Header";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import '../less/noteCreate.less';
import {createNote} from "../actions/noteCreateAction";

class NoteCreate extends React.Component{

  createNote(){
    const note = {
      title: document.getElementById('title').value,
      description: document.getElementById('description').value
    }
    this.props.createNote(note);
  }

  disabledLink(){
    const title = document.getElementById('title').value;
    const desc = document.getElementById('description').value;
    if (title === '' || desc === ''){
      return true;
    }
    return false;
  }

  render() {
    return (
      <section>
        <Header/>
        <div className={'createSection'}>
          <h1>创建标题</h1>
          <p className={'line'} />

          <label htmlFor={'title'}>标题</label>
          <input type={'text'} id={'title'} required/>

          <label htmlFor={'description'}>正文</label>
          <textarea className={'description'} id={'description'} required/>

          <div className={'operation'}>
            <div className={'operationBlock'}>
              <div aria-disabled={this.disabledLink} className={'create'} onClick={this.createNote.bind(this)}><Link to={'/'}>提交</Link></div>
              <div className={'return'}><Link to={'/'}>返回</Link></div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    notesList: state.notes.notesList
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  createNote
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NoteCreate);