export const createNote = (note) => (dispatch) => {
  fetch('http://localhost:8080/api/posts' ,{
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(note)
  }).then(res => {
    dispatch({
      type: 'CREATE_NOTE'
    });
  });
};