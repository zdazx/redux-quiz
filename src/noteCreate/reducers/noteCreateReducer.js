const initState ={

};

export default (state = initState, action) => {
  switch (action.type) {
    case 'CREATE_NOTE':
      return {
        ...state
      };
    default:
      return state;
  }
};