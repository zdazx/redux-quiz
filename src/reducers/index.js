import {combineReducers} from "redux";
import notesReducer from "../home/reducers/notesReducer";
import noteDetailReducer from "../noteDetail/reducers/noteDetailReducer";
import noteCreateReducer from "../noteCreate/reducers/noteCreateReducer";

const reducers = combineReducers({
  notes: notesReducer,
  noteDetail: noteDetailReducer,
  noteCreate: noteCreateReducer
});
export default reducers;