import React from 'react';

import {Link} from "react-router-dom";
import {MdLibraryAdd} from "react-icons/md";

const NotesTitleList = (props) => {
  const notes = props.notes;
  return (
    <div className={'TitleModule'}>
      {
        notes.map(item => {
          return <div className={'title'} key={item.id}><Link to={`/notes/${item.id}`}>{item.title}</Link></div>
        })
      }
      <div className={'title'}>
        <Link to={'/notes/create'}><MdLibraryAdd /></Link>
      </div>
    </div>
  );
};

export default NotesTitleList;