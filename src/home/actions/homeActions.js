export const getNotes = () => (dispatch) => {
  fetch('http://localhost:8080/api/posts')
    .then(response => response.json())
    .then(response => {
      console.log(response);
      dispatch({
        type: 'GET_NOTES',
        notesList: response
      });
    });
};