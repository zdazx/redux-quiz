const initState ={
  notesList: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_NOTES' :
      return {
        ...state,
        notesList: action.notesList
      };
    case 'GET_DETAIL':
      return {
        ...state,
        noteDetail: action.noteDetail
      };
    default:
      return state;
  }
};