import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getNotes} from "../actions/homeActions";
import Header from "../../header/Header";
import '../less/TitleModule.less'
import NotesTitleList from "../components/NotesTitleList";

class Home extends React.Component{
  componentDidMount() {
    this.props.getNotes();
  }

  getNoteDetail(note){
    this.props.getNoteDetail(note);
  }

  render() {
    const notesList = this.props.notesList;
    return (
      <section>
        <Header/>
        <NotesTitleList notes={notesList}/>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    notesList: state.notes.notesList
  }
};

const mapDispatchToProps = dispatch => bindActionCreators({
  getNotes
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);